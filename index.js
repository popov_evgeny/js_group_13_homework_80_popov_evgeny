const express = require('express');
const db = require('./mySqlDb');
const app = express();
const resources = require('./app/resources');
const category = require('./app/category');
const location = require('./app/location');

const port = 8000;

app.use(express.json());
app.use(express.static('public'));
app.use('/resources', resources);
app.use('/category', category);
app.use('/location', location);

const run = async () => {
  await db.init();

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });
}

run().catch(e => {
  console.error(e);
});
