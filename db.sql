create schema popov_evgeny collate utf8_general_ci;

use popov_evgeny;

create table category
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         null
);

create table location
(
    id          int auto_increment
        primary key,
    name        varchar(255) not null,
    description text         null
);

create table objects
(
    id          int auto_increment
        primary key,
    object_name varchar(255) not null,
    category_id int          not null,
    location_id int          not null,
    description text         null,
    date        datetime     null,
    image       varchar(31)  null,
    constraint object_category_id_fk
        foreign key (category_id) references category (id),
    constraint object_location_id_fk
        foreign key (location_id) references location (id)
);

insert into category (id, name, description)
values  (2, 'mebel2q', 'nana');

insert into location (id, name, description)
values  (1, 'Офис № 1', 'отдел торговли'),
        (2, 'mebel2вa', 'nanasAAaSв'),
        (3, 'mebel2qqq', 'nana');

insert into objects (id, object_name, category_id, location_id, description, date, image)
values  (1, 'проектор1211', 2, 2, 'модель APSON', '2022-02-01 08:09:29', 'J556BxeOZP-M8pP0vj0D0.jpg'),
        (29, 'проектор1211', 2, 2, 'модель APSON', '2022-02-01 08:09:29', 'J556BxeOZP-M8pP0vj0D0.jpg'),
        (30, 'проектор12', 2, 2, 'модель APSON', '2022-02-01 08:09:29', 'Xbbd1IRX1eJMrzXNd5Sr3.jpg');