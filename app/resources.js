const express = require('express');
const multer  = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../mySqlDb');

const router = express.Router();
const storage = multer.diskStorage({
  destination: (req, file, cd) => {
    cd(null, config.uploadPath);
  },
  filename: (req, file, cd) => {
    cd(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res, next) => {
  try{
    const [objects] = await db.getConnection().execute('SELECT * FROM objects');
    const objectsForUser = objects.map( obj => {
      return {
        id: obj.id,
        category_id: obj.category_id,
        location_id: obj.location_id,
        object_name: obj.object_name
      }
    })
    res.send(objectsForUser);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const [objects] = await db.getConnection().execute('SELECT * FROM objects WHERE id = ?', [req.params.id]);
    const object = objects[0];
    if (!object){
      res.status(500).send('Такого файла нет!');
    } else {
      res.send(object);
    }
  } catch (e) {
    res.status(500).send('Заполните пожалуйста поля!');
    next(e);
  }
});

router.post('/',  upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.category_id || !req.body.location_id || !req.body.object_name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const object = {
      category_id: req.body.category_id,
      location_id: req.body.location_id,
      object_name: req.body.object_name,
      description: null,
      date: null,
      image: null
    }

    if (req.body.date) {
      object.date = req.body.date;
    }

    if (req.body.description) {
      object.description = req.body.description;
    }

    if (req.file) {
      object.image = req.file.filename;
    }

    let query = 'INSERT INTO objects(category_id, location_id, object_name, description, date, image) VALUES (?, ?, ?, ?, ?, ?)';

    const [result] = await db.getConnection().execute(query, [
      object.category_id,
      object.location_id,
      object.object_name,
      object.description,
      object.date,
      object.image
    ]);

    const id = result.insertId;

    const postObject = {
      id: id,
      category_id: object.category_id,
      location_id: object.location_id,
      object_name: object.object_name,
      description: object.description,
      date: object.date,
      image: object.image
    }

    return res.send(postObject);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', upload.single('image'), async (req, res, next) => {
  try {
    if (!req.body.location_id || !req.body.category_id || !req.body.object_name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const putObject = {
      category_id: req.body.category_id,
      location_id: req.body.location_id,
      object_name: req.body.object_name,
      description: null,
      date: null,
      image: null
    }

    if (req.body.date) {
      putObject.date = req.body.date;
    }

    if (req.body.description) {
      putObject.description = req.body.description;
    }

    if (req.file) {
      putObject.image = req.file.filename;
    }

    let query = `UPDATE objects SET category_id = "${putObject.category_id}", location_id = "${putObject.location_id}", 
                                    object_name = "${putObject.object_name}", description = "${putObject.description}",
                                    date = "${putObject.date}", image = "${putObject.image}" WHERE id = "${req.params.id}"`;

    const [results] = await db.getConnection().execute(query);

    if (results.changedRows === 1) {
      const putObjectForUser = {
        id: req.params.id,
        category_id: putObject.category_id,
        location_id: putObject.location_id,
        object_name: putObject.object_name,
        description: putObject.description,
        date: putObject.date,
        image: putObject.image
      }

      return res.send(putObjectForUser);
    }else {
      return res.status(500).send('Файл не изменен!');
    }
  } catch (e) {
    res.status(500).send('Что-то пошлонетак!');
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try{
    const [result] = await db.getConnection().execute('DELETE FROM objects WHERE id = ?;', [req.params.id]);
    return res.send(result);
  } catch (e) {
    next(e);
  }
});

module.exports = router;