const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();


router.get('/', async (req, res, next) => {
  try{
    const [location] = await db.getConnection().execute('SELECT * FROM location');
    res.send(location);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const [locations] = await db.getConnection().execute('SELECT * FROM location WHERE id = ?', [req.params.id]);
    const location = locations[0];
    if (!location){
      res.status(500).send('Такого файла нет!');
    } else {
      res.send(location);
    }
  } catch (e) {
    res.status(500).send('Заполните пожалуйста поля!');
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const location = {
      name: req.body.name,
      description: null,
    }

    if (req.body.description) {
      location.description = req.body.description;
    }

    let query = 'INSERT INTO location (name, description) VALUES (?, ?)';

    const [result] = await db.getConnection().execute(query, [
      location.name,
      location.description
    ]);

    const id = result.insertId;

    const postLocation = {
      id: id,
      name: location.name,
      description: location.description
    }

    return res.send(postLocation);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try{
    const [result] = await db.getConnection().execute('DELETE FROM location WHERE id = ?;', [req.params.id]);
    return res.send(result);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    if (!req.body.name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const putLocation = {
      name: req.body.name,
      description: null
    }

    if (req.body.description) {
      putLocation.description = req.body.description;
    }

    let query = `UPDATE location SET name = "${putLocation.name}", description = "${putLocation.description}" WHERE id = "${req.params.id}"`;

    const [results] = await db.getConnection().execute(query);

    if (results.changedRows === 1){
      const putLocationForUser = {
        id: req.params.id,
        name: putLocation.name,
        description: putLocation.description
      }
      return res.send(putLocationForUser);
    }else {
      return res.status(500).send('Файл не изменен!');
    }
  } catch (e) {
    res.status(500).send('Что-то пошлонетак!');
    next(e);
  }
});
module.exports = router;