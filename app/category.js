const express = require('express');
const db = require('../mySqlDb');

const router = express.Router();


router.get('/', async (req, res, next) => {
  try{
    const [category] = await db.getConnection().execute('SELECT * FROM category');
    res.send(category);
  } catch (e) {
    next(e);
  }
});

router.get('/:id', async (req, res, next) => {
  try{
    const [categories] = await db.getConnection().execute('SELECT * FROM category WHERE id = ?', [req.params.id]);
    const category = categories[0];
    if (!category){
      res.status(500).send('Такого файла нет!');
    } else {
      res.send(category);
    }
  } catch (e) {
    res.status(500).send('Заполните пожалуйста поля!');
    next(e);
  }
});

router.post('/', async (req, res, next) => {
  try {
    if (!req.body.name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const category = {
      name: req.body.name,
      description: null,
    }

    if (req.body.description) {
      category.description = req.body.description;
    }


    let query = 'INSERT INTO category (name, description) VALUES (?, ?)';

    const [result] = await db.getConnection().execute(query, [
      category.name,
      category.description
    ]);

    const id = result.insertId;

    const postCategory = {
      id: id,
      name: category.name,
      description: category.description
    }

    return res.send(postCategory);
  } catch (e) {
    next(e);
  }
});

router.delete('/:id', async (req, res, next) => {
  try{
    const [result] = await db.getConnection().execute('DELETE FROM category WHERE id = ?;', [req.params.id]);
    return res.send(result);
  } catch (e) {
    next(e);
  }
});

router.put('/:id', async (req, res, next) => {
  try {
    if (!req.body.name) {
      return res.status(500).send('Заполните пожалуйста поля!');
    }

    const putCategory = {
      name: req.body.name,
      description: null
    }


    if (req.body.description) {
      putCategory.description = req.body.description;
    }

    let query = `UPDATE category SET name = "${putCategory.name}", description = "${putCategory.description}" WHERE id = "${req.params.id}"`;

    const [results] = await db.getConnection().execute(query);

    if (results.changedRows === 1) {
      const putCategoryForUser = {
        id: req.params.id,
        name: putCategory.name,
        description: putCategory.description
      }
      return res.send(putCategoryForUser);
    }else {
      return res.status(500).send('Файл не изменен!');
    }
  } catch (e) {
    res.status(500).send('Что-то пошлонетак!');
    next(e);
  }
});
module.exports = router;